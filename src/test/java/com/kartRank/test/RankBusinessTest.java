/**
 * 
 */
package com.kartRank.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.kartrank.business.RankBusiness;
import com.kartrank.exception.KartRankException;
import com.kartrank.util.KartRankUtils;
import com.kartrank.vo.LapVO;
import com.kartrank.vo.PilotResultVO;

public class RankBusinessTest {

	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#readLine(java.lang.String)}.
	 * @throws KartRankException 
	 */
	@Test
	public void testReadLine() throws KartRankException {
		String line = "23:49:08.277	038 � F.MASSA		1		1:02.852 	44,275";
		RankBusiness rankBusiness = new RankBusiness();
		LapVO lap = rankBusiness.readLine(line);
		
		LapVO correctLap = new LapVO();
		correctLap.setHour(KartRankUtils.setHour("23:49:08.277"));
		correctLap.setIdPilot(new Integer(38));
		correctLap.setPilot("F.MASSA");
		correctLap.setLapNumber(new Integer(1));
		correctLap.setLapTime(KartRankUtils.setHour("1:02.852"));
		correctLap.setLapAverageSpeedy(new Double(44.275));
		
		Assert.assertNotNull(lap);
		Assert.assertEquals(correctLap.getHour(), lap.getHour());
		Assert.assertEquals(correctLap.getIdPilot(), lap.getIdPilot());
		Assert.assertEquals(correctLap.getPilot(), lap.getPilot());
		Assert.assertEquals(correctLap.getLapNumber(), lap.getLapNumber());
		Assert.assertEquals(correctLap.getLapTime(), lap.getLapTime());
		Assert.assertEquals(correctLap.getLapAverageSpeedy(), lap.getLapAverageSpeedy());
	}

	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#separateByPilot(java.util.Map, java.lang.String)}.
	 * @throws KartRankException 
	 */
	@Test
	public void testSeparateByPilot() throws KartRankException {
		String line1 = "23:49:08.277	038 � F.MASSA	1	1:02.852	44,275";
		String line2 = "23:49:10.858	033 � R.BARRICHELLO	1	1:04.352	43,243";
		String line3 = "23:50:11.447	038 � F.MASSA	2	1:03.170	44,053";
		
		RankBusiness rankBusiness = new RankBusiness();
		LapVO lap1 = rankBusiness.readLine(line1);
		LapVO lap2 = rankBusiness.readLine(line2);
		LapVO lap3 = rankBusiness.readLine(line3);
		
		Map<Integer, List<LapVO>> lapsByPilot = new HashMap<Integer, List<LapVO>>();
		
		rankBusiness.separateByPilot(lapsByPilot, lap1);
		Assert.assertEquals(lapsByPilot.size(), 1);
		Assert.assertNotNull(lapsByPilot.get(lap1.getIdPilot()));
		Assert.assertEquals(lapsByPilot.get(lap1.getIdPilot()).size(), 1);
		
		rankBusiness.separateByPilot(lapsByPilot, lap2);
		Assert.assertEquals(lapsByPilot.size(), 2);
		Assert.assertNotNull(lapsByPilot.get(lap1.getIdPilot()));
		Assert.assertEquals(lapsByPilot.get(lap1.getIdPilot()).size(), 1);
		Assert.assertNotNull(lapsByPilot.get(lap2.getIdPilot()));
		Assert.assertEquals(lapsByPilot.get(lap2.getIdPilot()).size(), 1);
		
		rankBusiness.separateByPilot(lapsByPilot, lap3);
		Assert.assertEquals(lapsByPilot.size(), 2);
		Assert.assertNotNull(lapsByPilot.get(lap1.getIdPilot()));
		Assert.assertEquals(lapsByPilot.get(lap1.getIdPilot()).size(), 2);
		Assert.assertNotNull(lapsByPilot.get(lap2.getIdPilot()));
		Assert.assertEquals(lapsByPilot.get(lap2.getIdPilot()).size(), 1);
	}
	
	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#completeRace(java.util.List)}.
	 */
	@Test
	public void testCompleteRace() {
		List<LapVO> pilotLaps = Arrays.asList(new LapVO(), new LapVO(), new LapVO(), new LapVO());
		RankBusiness rankBusiness = new RankBusiness();
		boolean completeRace = rankBusiness.completeRace(pilotLaps);
		Assert.assertTrue(completeRace);
		
		pilotLaps = Arrays.asList(new LapVO(), new LapVO(), new LapVO());
		completeRace = rankBusiness.completeRace(pilotLaps);
		Assert.assertFalse(completeRace);
	}
	
	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#pilotTotalTime(java.util.List)}.
	 * @throws KartRankException 
	 */
	@Test
	public void testPilotTotalTime() throws KartRankException {
		LapVO lap1 = new LapVO();
		lap1.setLapTime(KartRankUtils.setHour("1:02.852"));
		LapVO lap2 = new LapVO();
		lap2.setLapTime(KartRankUtils.setHour("1:03.170"));
		LapVO lap3 = new LapVO();
		lap3.setLapTime(KartRankUtils.setHour("1:02.769"));
		LapVO lap4 = new LapVO();
		lap4.setLapTime(KartRankUtils.setHour("1:02.787"));
		List<LapVO> pilotLaps = new ArrayList<LapVO>();
		pilotLaps.add(lap1);
		pilotLaps.add(lap2);
		pilotLaps.add(lap3);
		pilotLaps.add(lap4);
		
		RankBusiness rankBusiness = new RankBusiness();
		Date totalTime = rankBusiness.pilotTotalTime(pilotLaps);
		
		Calendar calTotalTime = Calendar.getInstance();
		calTotalTime.setTime(totalTime);
		
		Assert.assertEquals(calTotalTime.get(Calendar.MINUTE), 4);
		Assert.assertEquals(calTotalTime.get(Calendar.SECOND), 11);
		Assert.assertEquals(calTotalTime.get(Calendar.MILLISECOND), 578);
	}
	
	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#orderPosition(java.util.List)}.
	 */
	@Test
	public void testOrderPosition() {
		PilotResultVO p1 = new PilotResultVO();
		p1.setIdPilot(1);
		p1.setTotalLaps(4);
		p1.setTotalTime(new Date());
		
		PilotResultVO p2 = new PilotResultVO();
		p2.setIdPilot(2);
		p2.setTotalLaps(4);
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.HOUR, 1);
		p2.setTotalTime(cal2.getTime());
		
		PilotResultVO p3 = new PilotResultVO();
		p3.setIdPilot(3);
		p3.setTotalLaps(4);
		Calendar cal3 = Calendar.getInstance();
		cal3.add(Calendar.HOUR, 2);
		p3.setTotalTime(cal3.getTime());
		
		PilotResultVO p4 = new PilotResultVO();
		p4.setIdPilot(4);
		p4.setTotalLaps(4);
		Calendar cal4 = Calendar.getInstance();
		cal4.add(Calendar.HOUR, 3);
		p4.setTotalTime(cal4.getTime());
		
		List<PilotResultVO> pilotsResult = new ArrayList<PilotResultVO>();
		pilotsResult.add(p3);
		pilotsResult.add(p1);
		pilotsResult.add(p2);
		pilotsResult.add(p4);
		
		RankBusiness rankBusiness = new RankBusiness();
		rankBusiness.orderPosition(pilotsResult);
		
		for (int i = 0; i < pilotsResult.size(); i++) {
			Assert.assertEquals(pilotsResult.get(i).getIdPilot(), new Integer(i+1));
		}
	}
	
	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#orderBestLap(java.util.List)}.
	 */
	@Test
	public void testOrderBestLap() {
		LapVO lap1 = new LapVO();
		lap1.setLapTime(new Date());
		
		LapVO lap2 = new LapVO();
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.HOUR, 1);
		lap2.setLapTime(cal2.getTime());
		
		LapVO lap3 = new LapVO();
		Calendar cal3 = Calendar.getInstance();
		cal3.add(Calendar.HOUR, 2);
		lap3.setLapTime(cal3.getTime());
		
		LapVO lap4 = new LapVO();
		Calendar cal4 = Calendar.getInstance();
		cal4.add(Calendar.HOUR, 3);
		lap4.setLapTime(cal4.getTime());
		
		List<LapVO> pilotLaps = new ArrayList<LapVO>();
		pilotLaps.add(lap4);
		pilotLaps.add(lap3);
		pilotLaps.add(lap2);
		pilotLaps.add(lap1);
		
		RankBusiness rankBusiness = new RankBusiness();
		rankBusiness.orderBestLap(pilotLaps);
		
		for (int i = 0; i < pilotLaps.size() -1; i++) {
			Assert.assertTrue(pilotLaps.get(i).getLapTime().getTime() < pilotLaps.get(i+1).getLapTime().getTime());
		}
	}
	
	/**
	 * Test method for {@link com.kartrank.business.RankBusiness#setPilotAverageSpeed(java.util.List)}.
	 */
	@Test
	public void testSetPilotAverageSpeed() {
		LapVO lap1 = new LapVO();
		lap1.setLapAverageSpeedy(44.275);
		
		LapVO lap2 = new LapVO();
		lap2.setLapAverageSpeedy(44.053);
		
		LapVO lap3 = new LapVO();
		lap3.setLapAverageSpeedy(44.334);
		
		LapVO lap4 = new LapVO();
		lap4.setLapAverageSpeedy(44.321);
		
		List<LapVO> pilotLaps = new ArrayList<LapVO>();
		pilotLaps.add(lap1);
		pilotLaps.add(lap2);
		pilotLaps.add(lap3);
		pilotLaps.add(lap4);
		
		RankBusiness rankBusiness = new RankBusiness();
		Double averageSpeed = rankBusiness.setPilotAverageSpeed(pilotLaps);
		
		Assert.assertEquals(averageSpeed, 44,24575);
	}
	
}
