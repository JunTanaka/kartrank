package com.kartRank.test;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.kartrank.exception.KartRankException;
import com.kartrank.util.KartRankUtils;

public class KartRankUtilsTest {

	/**
	 * Test method for {@link com.kartrank.business.KartRankUtils#setHour(java.lang.String)}.
	 * @throws KartRankException 
	 */
	@Test
	public void testSetHour() throws KartRankException {
		Date hour = KartRankUtils.setHour("23:49:08.277");
		
		Calendar correctHour = Calendar.getInstance();
		correctHour.set(Calendar.HOUR, 23);
		correctHour.set(Calendar.MINUTE, 49);
		correctHour.set(Calendar.SECOND, 8);
		correctHour.set(Calendar.MILLISECOND, 277);
		
		Calendar expectedHour = Calendar.getInstance();
		expectedHour.setTime(hour);
		
		Assert.assertEquals(correctHour, expectedHour);
	}
	
	/**
	 * Test method for {@link com.kartrank.business.KartRankUtils#setMinuteString(java.util.Date)}.
	 * @throws KartRankException 
	 */
	@Test
	public void testSetMinuteString() throws KartRankException {
		Date time = KartRankUtils.setHour("01:02.852");
		String minute = KartRankUtils.setMinuteString(time);
		
		Assert.assertEquals(minute, "01:02.852");
	}
}
