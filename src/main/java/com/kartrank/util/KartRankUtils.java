package com.kartrank.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.kartrank.exception.KartRankException;

public class KartRankUtils {
	
	private static Logger LOGGER = Logger.getLogger(KartRankUtils.class);

	public static Date setHour(String hours) throws KartRankException {
		Date settedHour = null;
		try {
			String[] hourSplit = hours.split(":");
			
			Integer hour = null;
			Integer minute = null;
			String[] secondAndMilis = null;
			if (hourSplit.length > 3) {
				LOGGER.error("Error in set hour. Non-standart time.");
				throw new KartRankException();
			}
			if (hourSplit.length == 1) {
				hour = 0;
				minute = 0;
				secondAndMilis = hourSplit[0].split("\\.");
			} else if (hourSplit.length == 2) {
				hour = 0;
				minute = Integer.parseInt(hourSplit[0]);
				secondAndMilis = hourSplit[1].split("\\.");
			} else {
				hour = Integer.parseInt(hourSplit[0]);
				minute = Integer.parseInt(hourSplit[1]);
				secondAndMilis = hourSplit[2].split("\\.");
			}
			
			Integer second = Integer.parseInt(secondAndMilis[0]);
			Integer milisecond = Integer.parseInt(secondAndMilis[1]);
			
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR, hour);
			cal.set(Calendar.MINUTE, minute);
			cal.set(Calendar.SECOND, second);
			cal.set(Calendar.MILLISECOND, milisecond);
		
			settedHour = cal.getTime();
		} catch (NumberFormatException e) {
			LOGGER.error("Error in set hour.", e);
			throw new KartRankException();
		}
		return settedHour;
	}

	public static Double setDouble(String value) {
		return new Double(value.replace(",", "."));
	}

	public static void resetTime(Calendar calTotalTime) {
		calTotalTime.set(Calendar.HOUR, 0);
		calTotalTime.set(Calendar.MINUTE, 0);
		calTotalTime.set(Calendar.SECOND, 0);
		calTotalTime.set(Calendar.MILLISECOND, 0);
	}
	
	public static Double doubleValuePrecision(Double value) {
		return BigDecimal.valueOf(value)
			.setScale(3, RoundingMode.HALF_UP)
			.doubleValue();
	}
	
	public static String setMinuteString(Date date) {
		return new SimpleDateFormat("mm:ss.SSS").format(date);
	}
	
}
