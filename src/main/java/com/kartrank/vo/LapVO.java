package com.kartrank.vo;

import java.util.Date;

public class LapVO implements Comparable<LapVO> {

	public Date hour;
	
	public Integer idPilot;
	
	public String pilot;
	
	public Integer lapNumber;
	
	public Date lapTime;
	
	public Double lapAverageSpeedy;

	/**
	 * @return the hour
	 */
	public Date getHour() {
		return hour;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(Date hour) {
		this.hour = hour;
	}

	/**
	 * @return the idPilot
	 */
	public Integer getIdPilot() {
		return idPilot;
	}

	/**
	 * @param idPilot the idPilot to set
	 */
	public void setIdPilot(Integer idPilot) {
		this.idPilot = idPilot;
	}

	/**
	 * @return the pilot
	 */
	public String getPilot() {
		return pilot;
	}

	/**
	 * @param pilot the pilot to set
	 */
	public void setPilot(String pilot) {
		this.pilot = pilot;
	}

	/**
	 * @return the lapNumber
	 */
	public Integer getLapNumber() {
		return lapNumber;
	}

	/**
	 * @param lapNumber the lapNumber to set
	 */
	public void setLapNumber(Integer lapNumber) {
		this.lapNumber = lapNumber;
	}

	/**
	 * @return the lapTime
	 */
	public Date getLapTime() {
		return lapTime;
	}

	/**
	 * @param lapTime the lapTime to set
	 */
	public void setLapTime(Date lapTime) {
		this.lapTime = lapTime;
	}

	/**
	 * @return the lapAverageSpeedy
	 */
	public Double getLapAverageSpeedy() {
		return lapAverageSpeedy;
	}

	/**
	 * @param lapAverageSpeedy the lapAverageSpeedy to set
	 */
	public void setLapAverageSpeedy(Double lapAverageSpeedy) {
		this.lapAverageSpeedy = lapAverageSpeedy;
	}

	public int compareTo(LapVO o) {
		if (this.lapTime.getTime() < o.getLapTime().getTime()) {
			return -1;
		}
		if (this.lapTime.getTime() > o.getLapTime().getTime()) {
			return 1;
		}
		return 0;
	}
}
