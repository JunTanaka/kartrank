package com.kartrank.vo;

import java.util.Date;

public class PilotTimeAfterWinner {

	private Integer idPilot;
	
	private String pilot;
	
	private Date timeAfterWinner;

	/**
	 * @return the idPilot
	 */
	public Integer getIdPilot() {
		return idPilot;
	}

	/**
	 * @param idPilot the idPilot to set
	 */
	public void setIdPilot(Integer idPilot) {
		this.idPilot = idPilot;
	}

	/**
	 * @return the pilot
	 */
	public String getPilot() {
		return pilot;
	}

	/**
	 * @param pilot the pilot to set
	 */
	public void setPilot(String pilot) {
		this.pilot = pilot;
	}

	/**
	 * @return the timeAfterWinner
	 */
	public Date getTimeAfterWinner() {
		return timeAfterWinner;
	}

	/**
	 * @param timeAfterWinner the timeAfterWinner to set
	 */
	public void setTimeAfterWinner(Date timeAfterWinner) {
		this.timeAfterWinner = timeAfterWinner;
	}
}
