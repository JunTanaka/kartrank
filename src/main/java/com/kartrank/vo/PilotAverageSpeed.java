package com.kartrank.vo;

public class PilotAverageSpeed {

	private Integer idPilot;
	
	private String pilot;
	
	private Double averageSpped;

	/**
	 * @return the idPilot
	 */
	public Integer getIdPilot() {
		return idPilot;
	}

	/**
	 * @param idPilot the idPilot to set
	 */
	public void setIdPilot(Integer idPilot) {
		this.idPilot = idPilot;
	}

	/**
	 * @return the pilot
	 */
	public String getPilot() {
		return pilot;
	}

	/**
	 * @param pilot the pilot to set
	 */
	public void setPilot(String pilot) {
		this.pilot = pilot;
	}

	/**
	 * @return the averageSpped
	 */
	public Double getAverageSpped() {
		return averageSpped;
	}

	/**
	 * @param averageSpped the averageSpped to set
	 */
	public void setAverageSpped(Double averageSpped) {
		this.averageSpped = averageSpped;
	}
}
