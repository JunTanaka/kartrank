package com.kartrank.vo;

import java.util.Date;

public class PilotResultVO implements Comparable<PilotResultVO>{
	
	private Integer idPilot;
	
	private String pilot;
	
	private Integer totalLaps;
	
	private Date totalTime;

	/**
	 * @return the idPilot
	 */
	public Integer getIdPilot() {
		return idPilot;
	}

	/**
	 * @param idPilot the idPilot to set
	 */
	public void setIdPilot(Integer idPilot) {
		this.idPilot = idPilot;
	}

	/**
	 * @return the pilot
	 */
	public String getPilot() {
		return pilot;
	}

	/**
	 * @param pilot the pilot to set
	 */
	public void setPilot(String pilot) {
		this.pilot = pilot;
	}

	/**
	 * @return the totalLaps
	 */
	public Integer getTotalLaps() {
		return totalLaps;
	}

	/**
	 * @param totalLaps the totalLaps to set
	 */
	public void setTotalLaps(Integer totalLaps) {
		this.totalLaps = totalLaps;
	}

	/**
	 * @return the totalTime
	 */
	public Date getTotalTime() {
		return totalTime;
	}

	/**
	 * @param totalTime the totalTime to set
	 */
	public void setTotalTime(Date totalTime) {
		this.totalTime = totalTime;
	}

	public int compareTo(PilotResultVO o) {
		if (this.totalLaps > o.getTotalLaps()) {
			return -1;
		}
		if (this.totalLaps < o.getTotalLaps()) {
			return 1;
		}
		if (this.totalTime.getTime() < o.getTotalTime().getTime()) {
			return -1;
		}
		if (this.totalTime.getTime() > o.getTotalTime().getTime()) {
			return 1;
		}
		return 0;
	}
}
