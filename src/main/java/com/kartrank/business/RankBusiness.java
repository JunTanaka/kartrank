package com.kartrank.business;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.kartrank.exception.KartRankException;
import com.kartrank.util.KartRankUtils;
import com.kartrank.vo.LapVO;
import com.kartrank.vo.PilotAverageSpeed;
import com.kartrank.vo.PilotResultVO;
import com.kartrank.vo.PilotTimeAfterWinner;

public class RankBusiness {
	
	private static Logger LOGGER = Logger.getLogger(RankBusiness.class);
	
	private static int TOTAL_LAPS = 4;
	
	public void raceResult() {
		Map<Integer, List<LapVO>> lapsByPilot = new HashMap<Integer, List<LapVO>>();
		List<PilotResultVO> pilotsResult = new ArrayList<PilotResultVO>();
		List<LapVO> pilotsBestLap = new ArrayList<LapVO>();
		LapVO bestLapOfRace = null;
		List<PilotAverageSpeed> pilotsAverageSpeed = new ArrayList<PilotAverageSpeed>();
		List<PilotTimeAfterWinner> pilotsTimeAfterWinner = new ArrayList<PilotTimeAfterWinner>();
		
		BufferedReader bufferedReader = null;
		String fileName = "kartRank.txt";
		try {
			bufferedReader = new BufferedReader(new FileReader(fileName));
			
			// Ignore header
			bufferedReader.readLine();
		    
			String line = bufferedReader.readLine();
		    while (line != null) {
		        LapVO pilotLap = readLine(line);
		        separateByPilot(lapsByPilot, pilotLap);
		        line = bufferedReader.readLine();
		    }
		    
		    for (Entry<Integer, List<LapVO>> entry : lapsByPilot.entrySet()) {
				List<LapVO> pilotLaps = entry.getValue();
				if (pilotLaps != null && !pilotLaps.isEmpty()) {
					orderBestLap(pilotLaps);
					pilotsBestLap.add(pilotLaps.get(0));
					
					PilotResultVO pilotResult = new PilotResultVO();
					pilotResult.setIdPilot(pilotLaps.get(0).getIdPilot());
					pilotResult.setPilot(pilotLaps.get(0).getPilot());
					pilotResult.setTotalLaps(pilotLaps.size());
					pilotResult.setTotalTime(pilotTotalTime(pilotLaps));
					pilotsResult.add(pilotResult);
					
					PilotAverageSpeed pilotAverageSpeed = new PilotAverageSpeed();
					pilotAverageSpeed.setIdPilot(pilotResult.getIdPilot());
					pilotAverageSpeed.setPilot(pilotResult.getPilot());
					pilotAverageSpeed.setAverageSpped(setPilotAverageSpeed(pilotLaps));
					pilotsAverageSpeed.add(pilotAverageSpeed);
				}
			}
		    
		    orderBestLap(pilotsBestLap);
		    bestLapOfRace = pilotsBestLap.get(0);
		    
		    orderPosition(pilotsResult);
		    Date winnerTotalTime = null; 
		    for (PilotResultVO pilotResult : pilotsResult) {
		    	PilotTimeAfterWinner pilotTimeAfterWinner = new PilotTimeAfterWinner();
		    	pilotTimeAfterWinner.setIdPilot(pilotResult.getIdPilot());
		    	pilotTimeAfterWinner.setPilot(pilotResult.getPilot());

		    	// Winner
		    	if (winnerTotalTime == null) {
		    		winnerTotalTime = pilotResult.getTotalTime();
		    		
		    		// Zero
		    		Calendar cal = Calendar.getInstance();
		    		KartRankUtils.resetTime(cal);
		    		pilotTimeAfterWinner.setTimeAfterWinner(cal.getTime());
		    	} else {
		    		pilotTimeAfterWinner.setTimeAfterWinner(new Date(pilotResult.getTotalTime().getTime() - winnerTotalTime.getTime()));
		    	}
		    	pilotsTimeAfterWinner.add(pilotTimeAfterWinner);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Input file not found. File: " + fileName, e);
		} catch (IOException e) {
			LOGGER.error("Error reading file. File: " + fileName, e);
		} catch (KartRankException e) {
			LOGGER.error("Error reading lines. File: " + fileName, e);
		} finally {
		    try {
		    	if (bufferedReader != null) {
		    		bufferedReader.close();
		    	}
			} catch (IOException e) {
				LOGGER.error("Error closing reader.", e);
				e.printStackTrace();
			}
		}
		
		writeOutputFile(pilotsResult, pilotsBestLap, bestLapOfRace, pilotsAverageSpeed, pilotsTimeAfterWinner);
	}
	
	public LapVO readLine(String line) throws KartRankException {
		LapVO lap = null;
		StringTokenizer strTokenizer = new StringTokenizer(line, "\t");
		if (strTokenizer.countTokens() == 5) {
			try {
				lap = new LapVO();
				lap.setHour(KartRankUtils.setHour(strTokenizer.nextToken().trim()));
				this.setPilotInformation(strTokenizer.nextToken(), lap);
				lap.setLapNumber(Integer.parseInt(strTokenizer.nextToken().trim()));
				lap.setLapTime(KartRankUtils.setHour(strTokenizer.nextToken().trim()));
				lap.setLapAverageSpeedy(KartRankUtils.setDouble(strTokenizer.nextToken().trim()));
			} catch (NumberFormatException e) {
				LOGGER.error("Error in read line. Invalid Number.", e);
				throw new KartRankException();
			}
		} else {
			LOGGER.error("Error in read line. Non-stardard line");
			throw new KartRankException();
		}
		
		return lap;
	}
	
	public void separateByPilot(Map<Integer, List<LapVO>> lapsByPilot, LapVO pilotLap) {
		if (lapsByPilot.get(pilotLap.getIdPilot()) != null) {
			lapsByPilot.get(pilotLap.getIdPilot()).add(pilotLap);
		} else {
			List<LapVO> lapByPilot = new ArrayList<LapVO>();
			lapByPilot.add(pilotLap);
			lapsByPilot.put(pilotLap.getIdPilot(), lapByPilot);
		}
	}
	
	public boolean completeRace(List<LapVO> pilotLaps) {
		boolean complete = false;
		if (pilotLaps.size() == TOTAL_LAPS) {
			complete = true;
		}
		return complete;
	}
	
	public Date pilotTotalTime(List<LapVO> pilotLaps) {
		Calendar calTotalTime = Calendar.getInstance();
		KartRankUtils.resetTime(calTotalTime);
		
		for (LapVO pilotLap : pilotLaps) {
			Calendar calLapTime = Calendar.getInstance();
			calLapTime.setTime(pilotLap.getLapTime());
			
			calTotalTime.add(Calendar.HOUR, calLapTime.get(Calendar.HOUR));
			calTotalTime.add(Calendar.MINUTE, calLapTime.get(Calendar.MINUTE));
			calTotalTime.add(Calendar.SECOND, calLapTime.get(Calendar.SECOND));
			calTotalTime.add(Calendar.MILLISECOND, calLapTime.get(Calendar.MILLISECOND));
		}
		
		return calTotalTime.getTime();
	}
	
	public void orderPosition(List<PilotResultVO> pilotsResult) {
		Collections.sort(pilotsResult);
	}
	
	public void orderBestLap(List<LapVO> pilotLaps) {
		Collections.sort(pilotLaps);
	}
	
	public Double setPilotAverageSpeed(List<LapVO> pilotLaps) {
		double sumAverageSpeed = 0;
		for (LapVO pilotLap : pilotLaps) {
			sumAverageSpeed += pilotLap.getLapAverageSpeedy();
		}
		return sumAverageSpeed / pilotLaps.size();
	}
	
	public void writeOutputFile(List<PilotResultVO> pilotsResult, List<LapVO> pilotsBestLap, LapVO bestLapOfRace, List<PilotAverageSpeed> pilotsAverageSpeed, List<PilotTimeAfterWinner> pilotsTimeAfterWinner) {
		BufferedWriter bw = null;
		FileWriter fw = null;
		String fileName = "kartResult.txt";

		try {
			StringBuilder sb = new StringBuilder();
			sb.append("Posi��o Chegada");
			sb.append("\t\t");
			sb.append("C�digo Piloto");
			sb.append("\t");
			sb.append("Nome Piloto");
			sb.append("\t");
			sb.append("Qtde Voltas Completadas");
			sb.append("\t");
			sb.append("Tempo Total de Prova");
			sb.append("\n");
			
			for (int i = 0; i < pilotsResult.size(); i++) {
				sb.append(i+1);
				sb.append("\t\t\t");
				sb.append(pilotsResult.get(i).getIdPilot());
				sb.append("\t\t");
				sb.append(pilotsResult.get(i).getPilot());
				if (pilotsResult.get(i).getPilot().length() < 8) {
					sb.append("\t\t");
				} else {
					sb.append("\t");
				}
				sb.append(pilotsResult.get(i).getTotalLaps());
				sb.append("\t\t\t");
				sb.append(KartRankUtils.setMinuteString(pilotsResult.get(i).getTotalTime()));
				sb.append("\n");
			}
			
			sb.append("\n");
			sb.append("Melhor Volta");
			sb.append("\n");
			for (LapVO pilotBestLap : pilotsBestLap) {
				sb.append(pilotBestLap.getPilot());
				if (pilotBestLap.getPilot().length() < 8) {
					sb.append("\t\t\t");
				} else {
					sb.append("\t\t");
				}
				sb.append(pilotBestLap.getLapNumber());
				sb.append("\t");
				sb.append(KartRankUtils.setMinuteString(pilotBestLap.getLapTime()));
				sb.append("\n");
			}
			
			sb.append("\n");
			sb.append("Melhor volta da corrida");
			sb.append("\n");
			sb.append(bestLapOfRace.getPilot());
			if (bestLapOfRace.getPilot().length() < 8) {
				sb.append("\t\t\t");
			} else {
				sb.append("\t\t");
			}
			sb.append(bestLapOfRace.getLapNumber());
			sb.append("\t");
			sb.append(KartRankUtils.setMinuteString(bestLapOfRace.getLapTime()));
			sb.append("\n");
			
			sb.append("\n");
			sb.append("Velocidade m�dia por piloto");
			sb.append("\n");
			for (PilotAverageSpeed pilotAverageSpeed : pilotsAverageSpeed) {
				sb.append(pilotAverageSpeed.getIdPilot());
				sb.append("\t");
				sb.append(pilotAverageSpeed.getPilot());
				if (pilotAverageSpeed.getPilot().length() < 8) {
					sb.append("\t\t");
				} else {
					sb.append("\t");
				}
				sb.append(KartRankUtils.doubleValuePrecision(pilotAverageSpeed.getAverageSpped()));
				sb.append("\n");
			}
			
			sb.append("\n");
			sb.append("Tempo ap�s vencedor");
			sb.append("\n");
			for (PilotTimeAfterWinner pilotTimeAfterWinner : pilotsTimeAfterWinner) {
				sb.append(pilotTimeAfterWinner.getIdPilot());
				sb.append("\t");
				sb.append(pilotTimeAfterWinner.getPilot());
				if (pilotTimeAfterWinner.getPilot().length() < 8) {
					sb.append("\t\t");
				} else {
					sb.append("\t");
				}
				sb.append(KartRankUtils.setMinuteString(pilotTimeAfterWinner.getTimeAfterWinner()));
				sb.append("\n");
			}
			
			fw = new FileWriter(fileName);
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());

			LOGGER.info("Output file created.");

		} catch (IOException e) {
			LOGGER.error("Error writing file. File: " + fileName, e);
		} finally {
			try {
				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {
				LOGGER.error("Error closing file. File: " + fileName, ex);
			}
		}
	}
	
	private void setPilotInformation(String pilot, LapVO lap) throws KartRankException {
		String[] pilotInformation = pilot.split("\\�", -1);
		try {
			if (pilotInformation.length == 2) {
				lap.setIdPilot(Integer.parseInt(pilotInformation[0].trim()));
				lap.setPilot(pilotInformation[1].trim());
			} else {
				throw new KartRankException();
			}
		} catch (NumberFormatException e) {
			LOGGER.error("Error in read pilot information. Id Pilot invalid.", e);
			throw new KartRankException();
		}
	}
}
