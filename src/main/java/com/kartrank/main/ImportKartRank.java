package com.kartrank.main;

import org.apache.log4j.Logger;

import com.kartrank.business.RankBusiness;

public class ImportKartRank {
	
	public static Logger LOGGER = Logger.getLogger(ImportKartRank.class);

	public static void main(String[] args) {
		RankBusiness rankBusiness = new RankBusiness();
		rankBusiness.raceResult();
	}
}
